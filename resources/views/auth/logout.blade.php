<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<script type="text/javascript" src="{{ asset(url('js/app.js')) }}"></script>
<script type="text/javascript">
	$(window).on('load',function(){
		 $('#logout-form').submit();
	});
</script>