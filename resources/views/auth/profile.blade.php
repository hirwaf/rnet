@extends('layouts.app')
@section('title', "RNET-21 - ".Auth::user()->name)
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Profile <small class="pull-right">This is your profile <label>{{ Auth::user()->name }}</label></small></div>
    <div class="panel-body">
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/profile') }}">
		    {{ csrf_field() }}

		    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		        <label for="name" class="col-md-4 control-label">Name</label>

		        <div class="col-md-6">
		            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : Auth::user()->name }}" autofocus>

		            @if ($errors->has('name'))
		                <span class="help-block">
		                    <strong>{{ $errors->first('name') }}</strong>
		                </span>
		            @endif
		        </div>
		    </div>

		    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

		        <div class="col-md-6">
		            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') ? old('email') : Auth::user()->email }}">

		            @if ($errors->has('email'))
		                <span class="help-block">
		                    <strong>{{ $errors->first('email') }}</strong>
		                </span>
		            @endif
		        </div>
		    </div>

		    <div class="form-group">
		    	<label class="col-md-4 control-label" for="telephones">Telephones</label>

		    	<div class="col-md-6">
		    		<input type="text" name="telephones" class="form-control" value="{{ old('telephones') }}">
		    		@if ($errors->has('telephones'))
		                <span class="help-block">
		                    <strong>{{ $errors->first('telephones') }}</strong>
		                </span>
		            @else
		            	 <span class="help-block">
	                    	<small class="text-mute">Separete your telephone numbers by</small> <b class="text-info">;</b>
		                </span>   
		            @endif
		    	</div>
		    </div>

		    <div class="form-group">
		    	<label class="col-md-4 control-label" for="position">Position</label>

		    	<div class="col-md-6">
		    		<input type="text" name="position" class="form-control" value="{{ old('position') }}">
		    		@if ($errors->has('position'))
		                <span class="help-block">
		                    <strong>{{ $errors->first('position') }}</strong>
		                </span>
		            @endif
		    	</div>
		    </div>

		    <div class="form-group">
		    	<label class="col-md-4 control-label" for="facebook">Social Networks</label>

		    	<div class="col-md-6">
		    		<label class="control-label" for="facebook">Facebook</label>
		    		<input type="text" name="social[facebook]" class="form-control" value="" id="facebook">
		    		<label class="control-label" for="twitter">Twitter</label>
		    		<input type="text" name="social[twitter]" class="form-control" value="" id="twitter">
		    		<label class="control-label" for="linkedin">Linkedin</label>
		    		<input type="text" name="social[linkedin]" class="form-control" value="" id="linkedin">
		    		<label class="control-label" for="googleplus">Google Plus</label>
		    		<input type="text" name="socail[googleplus]" class="form-control" value="" id="googleplus">
		    	</div>
		    </div>
		    <div class="form-group">
		    	<a href=""></a>
		    </div>
		    <div class="clearfix">&nbsp;</div>
		    <div class="form-group{{ $errors->has('current_password')? 'has-error' : '' }}" style="background-color: #ECF0F1; padding: 10px;">
		    	<label for="current-password" class="col-md-4 control-label">Current Password <span class="required">*</span></label>

		    	<div class="col-md-6">
		    		<input type="password" name="current_password" id="current-password" class="form-control">
		    		@if ($errors->has('current_password'))
		                <span class="help-block">
		                    <strong>{{ $errors->first('current_password') }}</strong>
		                </span>
		            @endif
		    	</div>
		    </div>
		    <div class="form-group">
		        <div class="col-md-6 col-md-offset-4">
		            <button type="submit" class="btn btn-primary">
		                Update
		            </button>
		        </div>
		    </div>
		</form>
    </div>
</div>
@endsection
