@extends('layouts.app')
@section('title', "RNET-21 - ".Auth::user()->name)

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		{{ $page->name }} Page &nbsp;&nbsp;&nbsp;
		<a href="{{ url(route('create.page')) }}" class="btn btn-xs btn-flat btn-primary">Add New</a> 
		<small class="pull-right"><label>{{ Auth::user()->name }}</label></small>
	</div>
	<div class="panel-body">
		@include("auth.pages.includes.".$view)
	</div>
</div>
@endsection

@section('styles')	
<link rel="stylesheet" href="{{asset(url('plugins/jQuery.filer/css/jquery.filer.css'))}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset(url('plugins/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css'))}}" rel="stylesheet" />
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset(url('js/jquery.min.js')) }}"></script>
<script src="{{ asset(url('plugins/jQuery.filer/js/jquery.filer.min.js?v=1.0.5')) }}"></script>
<script src="{{ asset(url('plugins/jQuery.filer/js/custom.js?v=1.0.5')) }}"></script>
@endsection