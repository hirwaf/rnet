@extends('layouts.app')
@section('title', "RNET-21 - ".Auth::user()->name)

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		Pages &nbsp;&nbsp;&nbsp;
		<a href="{{ url(route('create.page')) }}" class="btn btn-xs btn-flat btn-primary">Add New</a> 
		<small class="pull-right"><label>{{ Auth::user()->name }}</label></small>
	</div>
	<div class="panel-body">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>In Menu</th>
					<th width="30px"></th>
					<th width="30px"></th>
				</tr>
			</thead>
			<tbody>
				@if( count($pages) > 0 )
					@foreach( $pages as $page )
						<tr>
							<td><label class="label label-primary">{{ $page->order }}</label></td>
							<td><label class="label label-primary">{{ $page->name }}</label></td>
							<td><label class="label label-primary">{{ $page->menu ? "Yes" : "No" }}</label></td>
							<td>
								<a href="{{ url(route('edit.page',$page->id).'?_page='.session()->get('pagEdit') ) }}" class="btn btn-sm btn-info pull-right">Edit</a>
							</td>
							<td>
								<a onclick="if (confirm('Are Sure !!\n') != true) { return false }
											" href="{{ url(route('destroy.page',$page->id)) }}" class="btn btn-sm btn-{{ $page->active ? 'danger' : 'success' }}  pull-right">{{ $page->active ? "Disable" : "Enable" }}</a>
							</td>
						</tr>
					@endforeach
					<tr>
						<td colspan="6">
							<nav>
							  <ul class="pager">
							    <li><a href="{{ $pages->previousPageUrl() }}">Previous</a></li>
							    <li><a href="{{ $pages->nextPageUrl() }}">Next</a></li>
							  </ul>
							</nav>
						</td>
					</tr>
				@else
					<tr>
						<td colspan="5">
							<h4>There are pages in Database !!</h4>
						</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('styles')	
@endsection

@section('scripts')
@endsection