<div class="row">
	<div class="col-md-12">
		
	</div>
	<div class="col-md-12">
		<form role="form" method="post" action="{{ url(route('update.page',$page->id)) }}">
			{{ csrf_field() }}
			<div class="from-group">
				<label class="control-label col-md-2">Upload New</label>
				<div class="col-md-10">
					<input type="file" name="image" id="image">
				</div>
			</div>
		</form>
	</div>
</div>
