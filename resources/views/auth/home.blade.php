@extends('layouts.app')
@section('title', "RNET-21 - ".Auth::user()->name)
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Dashboard <small class="pull-right">Welcome back <label>{{ Auth::user()->name }}</label></small></div>
    <div class="panel-body">
        You are logged in!
    </div>
</div>
@endsection
