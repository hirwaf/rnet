@extends('layouts.app')
@section('title', "RNET-21 - ".Auth::user()->name)
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Edit Password <small class="pull-right"><label>{{ Auth::user()->name }}</label></small></div>
    <div class="panel-body">
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/password') }}">
		    {{ csrf_field() }}
		    <div class="row">
			    <div class="col-md-12">
				    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
		                <label for="password" class="col-md-4 control-label">New Password <span class="required">*</span></label>

		                <div class="col-md-6">
		                    <input id="password" type="password" class="form-control" name="password" required>

		                    @if ($errors->has('password'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('password') }}</strong>
		                        </span>
		                    @endif
		                </div>
		            </div>

		            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
		                <label for="password-confirm" class="col-md-4 control-label">Confirm Password <span class="required">*</span></label>

		                <div class="col-md-6">
		                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

		                    @if ($errors->has('password_confirmation'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('password_confirmation') }}</strong>
		                        </span>
		                    @endif
		                </div>
		            </div>	
			    </div>
            </div>
            <div class="row">&nbsp;</div>
		    <div class="row" style="background-color: #ECF0F1;">
			    <div class="col-md-12" style="padding: 1em;">
				    <div class="form-group{{ $errors->has('current_password')?  ' has-error' : '' }}">
				    	<label for="current-password" class="col-md-4 control-label">Current Password <span class="required">*</span></label>

				    	<div class="col-md-6">
				    		<input type="password" name="current_password" id="current-password" class="form-control" required>
				    		@if ($errors->has('current_password'))
				                <span class="help-block">
				                    <strong>{{ $errors->first('current_password') }}</strong>
				                </span>
				            @endif
				    	</div>
				    </div>		
			    </div>
		    </div>
		    <div class="clearfix">&nbsp;</div>
		    <div class="form-group">
		        <div class="col-md-6 col-md-offset-4">
		            <button type="submit" class="btn btn-primary">
		                Update
		            </button>
		        </div>
		    </div>
		</form>
    </div>
</div>
@endsection

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset(url('css/plugins.css')) }}">
@endsection

@section('scripts')
	<!-- PNotify -->
	<script type="text/javascript" src="{{ asset(url('plugins/notify/pnotify.core.js')) }}"></script>
	<script type="text/javascript" src="{{ asset(url('plugins/notify/pnotify.buttons.js')) }}"></script>
	<script type="text/javascript" src="{{ asset(url('plugins/notify/pnotify.nonblock.js')) }}"></script>
	<!-- form validation -->
  	<script type="text/javascript" src="{{ asset(url('plugins/validator/validator.js')) }}"></script>
	<script type="text/javascript" src="{{ asset(url('js/custom.js')) }}" ></script>
	@if( $errors->has('current_password') )
		<script type="text/javascript">
			$(function(){
				new PNotify({
			        title: "Incorrect Password",
			        type: "error",
			        text: "{{ $errors->first('current_password') }}",
			        hidden: true,
			        nonblock: {
			          nonblock: true
			        }
			    });
			});
		</script>	
	@endif
	@if( $errors->has('fail') )
		<script type="text/javascript">
			$(function(){
				new PNotify({
			        title: "Incorrect Password",
			        type: "error",
			        text: "{{ $errors->first('fail') }}",
			        hidden: true,
			        nonblock: {
			          nonblock: true
			        }
			    });
			});
		</script>	
	@endif
	@if( session()->has('fail') )
        <script type="text/javascript">
            $(function(){
                new PNotify({
                    title: "Confirmation",
                    type: "info",
                    text: " {{ session()->pull('fail') }}",
                    hidden: true,
                    nonblock: {
                      nonblock: true
                    }
                });
            });
        </script>   
    @endif
@endsection