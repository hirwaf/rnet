@extends('layouts.app')
@section('title', 'RNET-21 | Login')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus autocomplete="off">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(url('css/plugins.css')) }}">
@endsection

@section('scripts')
    <!-- PNotify -->
    <script type="text/javascript" src="{{ asset(url('plugins/notify/pnotify.core.js')) }}"></script>
    <script type="text/javascript" src="{{ asset(url('plugins/notify/pnotify.buttons.js')) }}"></script>
    <script type="text/javascript" src="{{ asset(url('plugins/notify/pnotify.nonblock.js')) }}"></script>
    <!-- form validation -->
    <script type="text/javascript" src="{{ asset(url('plugins/validator/validator.js')) }}"></script>
    <script type="text/javascript" src="{{ asset(url('js/custom.js')) }}" ></script>
    @if( session()->has('fail') )
        <script type="text/javascript">
            $(function(){
                new PNotify({
                    title: "Confirmation",
                    type: "info",
                    text: " {{ session()->pull('fail') }}",
                    hidden: true,
                    nonblock: {
                      nonblock: true
                    }
                });
            });
        </script>   
    @endif
@endsection