<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset(url('css/app.css')) }}" rel="stylesheet">
    <link href="{{ asset(url('css/custorm.css')) }}" rel="stylesheet">
    @yield('styles')
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ Auth::check() ? url('/dashboard') : url('/') }}">
                    RNET-21
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/profile') }}">Profile</a>
                                    <a href="{{ url('/password') }}">Change Password</a>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            @if ( ! Auth::guest())
                <div class="col-md-3" id="sidebar">
                    <div class="row visible-md visible-lg">
                        <div class="col-md-12 header">
                            <div class="img img-responsive">
                                @if( Auth::user()->info() )
                                    @if( Auth::user()->info()->picture != null )
                                        <img src="">
                                    @endif
                                @else
                                    <h1 class="text-center"> {{ strtoupper(Auth::user()->name{0}) }} </h1>    
                                @endif
                            </div>
                            <div class="name">{{ Auth::user()->name }}</div>    
                        </div>
                    </div>
                    <div class="row">
                        <div>&nbsp;</div>
                        <div class="col-md-12">
                            @include('auth.sidebar')
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                </div>
            @endif
            <div class="col-md-8">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset(url('js/app.js')) }}"></script>
    @yield('scripts')
</body>
</html>
