<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RNET-21 - LET'S BE YOUR PARTNER </title>
    <!-- Google Font -->
    <link href='http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Font Awesome -->


    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

     <!-- Preloader -->
    <link rel="stylesheet" href="css/preloader.css" type="text/css" media="screen, print"/>

    <!-- Icon Font-->
    <link rel="stylesheet" href="{{ asset(url('css/style-fonts.css')) }}">
    <link rel="stylesheet" href="{{ asset(url('css/owl.carousel.css')) }}">
    <link rel="stylesheet" href="{{ asset(url('css/owl.theme.default.css')) }}">
    <!-- Animate CSS-->
    <link rel="stylesheet" href="{{ asset(url('css/animate.css')) }}">

    <!-- Bootstrap -->
    <link href="{{ asset(url('css/bootstrap.min.css')) }}" rel="stylesheet">
    <!-- Style -->
    <link href="{{ asset(url('css/style.css')) }}" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="{{ asset(url('css/responsive.css')) }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="js/lte-ie7.js"></script>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="{{ asset(url('css/custom.css')) }}" rel="stylesheet">
</head>

<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>




    <header id="HOME" style="background-position: 50% -125px;">
            <div class="section_overlay">
                <nav class="navbar navbar-default navbar-fixed-top">
                  <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <!-- <a class="navbar-brand" href="#"><img src="{{ asset(url('images/logo.png')) }}" alt=""></a> -->
                      <div class="navbar-brand ourname"
                        style="
                            color: #f73b56;
                            font-weight: bolder;
                            font-size: x-large;
                            font-size: 37px;
                        "    
                        >RNET-21</div>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="#HOME">Home</a></li>
                        <li><a href="#SERVICE">Services</a></li>
                        <li><a href="#ABOUT">About</a></li>
                        <li><a href="#CONTACT">Contact</a></li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container -->
                </nav> 

                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="home_text wow fadeInUp animated">
                                <h2>Let's be your partner</h2>
                                <p>Innovation starts with us</p>
                                <img src="{{ asset(url('images/shape.png')) }}" alt="">                        
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="scroll_down">
                            <a href="#SERVICE"><img src="{{ asset(url('images/scroll.png')) }}" alt=""></a>
                                <h4>Scroll Down</h4>
                            </div>

                        </div>
                    </div>
                </div>            
            </div>    
    </header>
    <section class="call_to_action">
        <div class="container">
            <div class="row">
                <div class="col-md-8 wow fadeInLeft animated">
                    <div class="left">
                        <h2>LOOKING FOR EXCLUSIVE DIGITAL SERVICES?</h2>
                        <p>Technology has never been easy. Let <b>Rnet21</b>  
                        take you to the 21<sup>st</sup> century technology.<br/>we have the First Caliber in software development at affordable prices ever</p>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-1 wow fadeInRight animated">
                    <div class="baton">
                        <a href="#CONTACT">
                            <button type="button" class="btn btn-primary cs-btn">Let's Talk</button>
                        </a>    
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="services dark parallex transparent " id="SERVICE" style="
        background-image: url('{{ asset(url('/images/kks.jpg')) }}');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
        background-attachment: fixed;
        background-clip: all;
        opacity: .9;
        padding: 5em;
    " >
        <div class="wallcolor">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="services_title">
                            <h2>Services</h2>
                            <img src="images/shape.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="single_service card wow fadeInUp" data-wow-delay="1s">
                            <i class="glyphicon glyphicon-globe"></i>
                            <h2>Domain Name Registration</h2>
                            <p>Best domain names at affordable prices. like .com .org .club .net .... </p>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="single_service card wow fadeInUp" data-wow-delay="2s">
                            <i class="glyphicon glyphicon glyphicon-tasks"></i>
                            <h2>Web<br> Hosting</h2>
                            <p>Getting the unlimited space when hosting is our priority .</p>
                        </div>
                    </div>
                    <div class="col-md-3 text-center ">
                        <div class="single_service card wow fadeInUp" data-wow-delay="3s">
                            <i class="glyphicon glyphicon-indent-left"></i>
                            <h2>Web Development</h2>
                            <p>When developers starts the whole team stands still.</p>
                        </div>
                    </div>
                    <div class="col-md-3 text-center ">
                        <div class="single_service card wow fadeInUp" data-wow-delay="4s">
                            <i class="glyphicon glyphicon-asterisk"></i>
                            <h2>Software Development</h2>
                            <p>Nowadays it's a must for us to deliver trusted software </p>
                        </div>
                    </div>
                    <div class="col-md-3 text-center ">&nbsp;</div>
                    <div class="col-md-3 text-center ">
                        <div class="single_service card wow fadeInUp" data-wow-delay="4s">
                            <i class="glyphicon glyphicon-phone"></i>
                            <h2>Mobile Application</h2>
                            <p>We are the best mobile app developpers get yours now.</p>
                        </div>
                    </div>
                    <div class="col-md-3 text-center ">
                        <div class="single_service card wow fadeInUp" data-wow-delay="5s">
                            <i class="glyphicon glyphicon-link"></i>
                            <h2>General IT Consultancy</h2>
                            <p>Our consultation opens a golden bridge to our Customers.</p>
                        </div>
                    </div>

                </div>            
            </div>
        <div>
    </section>
    <!--
    <section class="about_us_area" id="ABOUT">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="about_title">
                        <h2>About Me</h2>
                        <img src="images/shape.png" alt="">
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-4  wow fadeInLeft animated">
                    <div class="single_progress_bar">
                        <h2>DESIGN - 90%</h2>
                        <div class="progress">
                          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                            <span class="sr-only">60% Complete</span>
                          </div>
                        </div>
                    </div>
                    <div class="single_progress_bar">
                        <h2>DEVELOPMENT - 60%</h2>
                        <div class="progress">
                          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                            <span class="sr-only">60% Complete</span>
                          </div>
                        </div>
                    </div>
                    <div class="single_progress_bar">
                        <h2>MARKETING - 75%</h2>
                        <div class="progress">
                          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
                            <span class="sr-only">60% Complete</span>
                          </div>
                        </div>
                    </div>
                    <div class="single_progress_bar">
                        <h2>SEO - 95%</h2>
                        <div class="progress">
                          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                            <span class="sr-only">60% Complete</span>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4  wow fadeInRight animated">
                    <p class="about_us_p">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Sed quia non numquam eius modi tempora.</p>
                </div>
                <div class="col-md-4  wow fadeInRight animated">
                    <p class="about_us_p">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                </div>
            </div>
        </div>
    </section>
    -->
    
    <section class="call_to_action">
        <div class="container">
            <div class="row">
                <div class="col-md-8 wow fadeInLeft animated">
                    <div class="left">
                        <h2>LOOKING FOR EXCLUSIVE DIGITAL SERVICES?</h2>
                        <p>Technology has never been easy. Let <b>Rnet21</b>  
                        take you to the 21<sup>st</sup> century technology.<br/>we have the First Caliber in software development at affordable prices ever</p>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-1 wow fadeInRight animated">
                    <div class="baton">
                        <a href="#CONTACT">
                            <button type="button" class="btn btn-primary cs-btn">Let's Talk</button>
                        </a>    
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="contact" id="CONTACT" style="
        /*background-image: url('{{ asset(url('/images/kkk.jpg')) }}');*/
        /*background-color: #2C3E50;*/
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
        background-attachment: fixed;
        background-clip: all;
        opacity: .9;
        padding: 5em;
    ">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="contact_title  wow fadeInUp animated">
                        <h1>get in touch with us</h1>
                        <img src="{{ asset(url('images/shape.png')) }}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3  wow fadeInLeft animated">
                    <div class="single_contact_info">
                        <h2>Call Us</h2>
                        <p>+250 785 668 910 </p>
                    </div>
                    <div class="single_contact_info">
                        <h2>Email Us</h2>
                        <p>info@rnet21.com</p>
                    </div>
                    <div class="single_contact_info">
                        <h2>Address</h2>
                        <p>Umuganda Street Address, Kacyiru, Eden Galden Building</p>
                    </div>
                </div>
                <div class="col-md-9  wow fadeInRight animated">
                    <form class="contact-form" action="mail:info@rnet21.com">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="name" placeholder="Name" required>
                                <input type="email" class="form-control" id="email" placeholder="Email" required>
                                <input type="text" class="form-control" id="subject" placeholder="Subject" required>                              
                            </div>
                            <div class="col-md-6">
                                <textarea class="form-control" id="message" rows="25" cols="10" placeholder="  Message Texts..."></textarea>
                                <button type="submit" class="btn btn-default submit-btn form_submit">SEND MESSAGE</button>                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="work-with   wow fadeInUp animated">
                        <h3>looking forward to hearing from you!</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>



<footer>
    <div class="container">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="footer_logo   wow fadeInUp animated">
                        <div class="ourname"
                        style="
                            color: #f73b56;
                            font-weight: bolder;
                            font-size: x-large;
                            font-size: 37px;
                        "    
                        >RNET-21</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center   wow fadeInUp animated">
                    <div class="social">
                        <h2>Follow Us on Here</h2>
                        <ul class="icon_list">
                            <li><a href="http://www.facebook.com/abdullah.noman99"target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="http://www.twitter.com/absconderm"target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="http://www.dribbble.com/abdullahnoman"target="_blank"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="copyright_text   wow fadeInUp animated">
                        <p>&copy; rnet 2016.All Right</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- =========================
     SCRIPTS 
============================== -->


<script src="{{ asset(url('js/jquery.min.js')) }}"></script>
<script src="{{ asset(url('js/bootstrap.min.js')) }}"></script>
<script src="{{ asset(url('js/jquery.nicescroll.js')) }}"></script>
<script src="{{ asset(url('js/owl.carousel.js')) }}"></script>
<script src="{{ asset(url('js/wow.js')) }}"></script>
<script src="{{ asset(url('js/script.js')) }}"></script>

</body>

</html>