<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
	return redirect(url('/'));
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index');

Route::group(['middleware' => 'auth'], function(){
	// Get Routes
	Route::get('/profile','HomeController@editProfile');
	Route::get('/password','HomeController@editPassword');
	Route::get('/logout', function(){
		return view('auth.logout');
	});
	// Post Routes
	Route::post('/profile', 'HomeController@updateProfile');
	Route::post('/password', 'HomeController@updatePassword');

	// Namespaces Auth Routes

	Route::group([ 'namespace' => 'Auth' ], function(){
		Route::resource('/pages', 'PageController', [
			'names'	=>	[
				'index'		=>	'pages',
				'create'	=>	'create.page',
				'edit'		=>	'edit.page',
				'show'		=>	'show.page',
				'store'		=>	'store.page',
				'update'	=>	'update.page',
				'destroy'	=>	'destroy.page',
			],
		]);
	});
});