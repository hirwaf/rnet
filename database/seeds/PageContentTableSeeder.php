<?php

use Illuminate\Database\Seeder;

use App\PageContent;

class PageContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	/* Home */
        PageContent::create([
        	'page_id'	=>	1,
        	'meta_key'	=>	'home_image',
        	'content'	=>	null
        ]);
        PageContent::create([
        	'page_id'	=>	1,
        	'meta_key'	=>	'home_text',
        	'content'	=>	json_encode(['IT’S ABDULLAH NOMAN','A USER INTERFACE AND USER EXPERIENCE SPECIALIST'])
        ]);

        /* Services */
        PageContent::create([
        	'page_id'	=>	2,
        	'meta_key'	=>	'services',
        	'content'	=>	json_encode(['DESIGN','Excepteur sint occaecat cupidatat non proident, sunt in culpa.']),
        ]);
        PageContent::create([
        	'page_id'	=>	2,
        	'meta_key'	=>	'services',
        	'content'	=>	json_encode(['DEVELOPMENT','Excepteur sint occaecat cupidatat non proident, sunt in culpa.']),
        ]);
        PageContent::create([
        	'page_id'	=>	2,
        	'meta_key'	=>	'services',
        	'content'	=>	json_encode(['PHOTOGRAPHY','Excepteur sint occaecat cupidatat non proident, sunt in culpa.']),
        ]);
        PageContent::create([
        	'page_id'	=>	2,
        	'meta_key'	=>	'services',
        	'content'	=>	json_encode(['SEO','Excepteur sint occaecat cupidatat non proident, sunt in culpa.']),
        ]);

        /* About Us */
        PageContent::create([
        	'page_id'	=>	3,
        	'meta_key'	=>	'text_1',
        	'content'	=>	"
	    		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Sed quia non numquam eius modi tempora.
        	"
        ]);

        PageContent::create([
        	'page_id'	=>	3,
        	'meta_key'	=>	'text_2',
        	'content'	=>	"
	    		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Sed quia non numquam eius modi tempora.
        	"
        ]);
        

    }
}
