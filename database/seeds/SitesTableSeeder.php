<?php

use Illuminate\Database\Seeder;
use App\Site;

class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Site::create([
        	'name'	=>	'sitename',
        	'value'	=>	'Rnet-21',
        ]);

        Site::create([
        	'name'	=>	'sitelogo',
        	'value'	=>	'favicon.jpg',
        ]);

        Site::create([
        	'name'	=>	'tag',
        	'value'	=>	'website;web hosting;domain name;domain registration;software;developer;software developer;system;web application;android application;'
        ]);

        Site::create([
        	'name'	=>	'slogan',
        	'value'	=>	"let's be your partner innovation starts with us"
        ]);

        Site::create([
        	'name'	=>	'email',
        	'value'	=>	null
        ]);

        Site::create([
        	'name'	=>	'telephone',
        	'value'	=>	'250 7898 12404'
        ]);

        Site::create([
        	'name'	=>	'facebook',
        	'value'	=>	null
        ]);

        Site::create([
        	'name'	=>	'twitter',
        	'value'	=>	null
        ]);

        Site::create([
        	'name'	=>	'linkedin',
        	'value'	=>	null
        ]);

        Site::create([
        	'name'	=>	'googleplus',
        	'value'	=>	null
        ]);

        Site::create([
        	'name'	=>	'instagram',
        	'value'	=>	null
        ]);

        Site::create([
        	'name'	=>	'facebook',
        	'value'	=>	null
        ]);

        Site::create([
        	'name'	=>	'address',
        	'value'	=>	"Kacyiru, Umuganda road Address"
        ]);

        Site::create([
        	'name'	=>	'description',
        	'value'	=>	'Some demo text here for you to see'
        ]);

    }
}
