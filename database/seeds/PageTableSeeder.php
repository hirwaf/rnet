<?php

use Illuminate\Database\Seeder;
use App\Page;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::create([
        	'name'		=>	'HOME',
        	'slug'		=>	'',
        	'order'		=>	1,
        	'menu'		=>	true,
        	'active'	=>	true	
        ]);
        Page::create([
        	'name'		=>	'SERVICES',
        	'slug'		=>	'services',
        	'order'		=>	2,
        	'menu'		=>	true,
        	'active'	=>	true	
        ]);
        Page::create([
        	'name'		=>	'ABOUT',
        	'slug'		=>	'about',
        	'order'		=>	3,
        	'menu'		=>	true,
        	'active'	=>	true	
        ]);
        Page::create([
        	'name'		=>	'CONTACT',
        	'slug'		=>	'contact',
        	'order'		=>	4,
        	'menu'		=>	true,
        	'active'	=>	true	
        ]);
        Page::create([
        	'name'		=>	'TEAM',
        	'slug'		=>	'team',
        	'order'		=>	5,
        	'menu'		=>	true,
        	'active'	=>	true	
        ]);

    }
}
