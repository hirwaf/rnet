<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //

    protected $fillable = [
        'name', 
        'slug', 
        'menu', 
        'active', 
        'order'
    ];

    public function page_content()
    {
    	$this->hasMany('App\PageContent');
    }
}
