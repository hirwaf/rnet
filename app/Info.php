<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{

	protected $fillable = [
        'user_id', 
        'telephones', 
        's_networks', 
        'pictures', 
        'position', 
        'knowledge'
    ];

    public function user()
    {
    	$this->belongsTo('App\User');
    }
}
