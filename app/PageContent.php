<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageContent extends Model
{
	use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $fillable = [
    	'page_id',
    	'content',
    	'meta_key'
    ];

    protected $table = "page_contents";

    public function page()
    {
    	$this->belongsTo('App\Page');
    }
}
