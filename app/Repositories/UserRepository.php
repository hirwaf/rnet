<?php
namespace App\Repositories;

use App\User;
use App\Info;
use Auth;
use Hash;

class UserRepository
{
	protected $id;

	public function __construct()
	{
		$this->id = Auth::check() ? Auth::user()->id : null;
	}

	public function updatePassword($request)
	{
		$user = User::find($this->id)->update([
			'password'	=>	Hash::make($request->password)
		]);

		if( $user )
			return true;
		else
			return false;
	}

	public function updateProfile($request)
	{
		$user = "";
	}

	protected function upload($file,$size=[],$return = "")
	{

	}

}