<?php
namespace App\Repositories;

use App\Page;
use Auth;

/**
* 
*/
class PageRepository
{
	
	function __construct()
	{
		# code...
	}

	public function getAllPages($p = 15)
	{
		return Page::orderBy('order','asc')
					->paginate($p);
	}

}