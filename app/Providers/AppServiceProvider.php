<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Auth;
use Hash;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('isme', function($attribute, $value, $parameters, $validator) {
            if( Auth::check() ){
                if( Hash::check( $value, Auth::user()->password ) )
                    return true;
            }
            else
                return false;
                
            
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
