<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\UpdatePassword;
use App\Http\Requests\UpdateProfile;

use App\Repositories\UserRepository;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('auth.home');
    }

    public function editProfile(){
        return view('auth.profile');
    }

    public function editPassword()
    {
        return view('auth.editpassword');
    }

    public function updatePassword(UpdatePassword $request, UserRepository $user)
    {
        $user = $user->updatePassword($request);
        if( $user ){
            session(['fail' => 'You have successfully changed your password']);
            return redirect(url('/password'));
        }
        else
            return redirect(url('/password'))->withErrors(['fail' => 'Fail please try again later!!']);
    }

    public function updateProfile(UpdateProfile $request, UserRepository $user)
    {

    }

}
